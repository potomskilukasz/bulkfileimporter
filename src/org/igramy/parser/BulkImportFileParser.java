package org.igramy.parser;

public interface BulkImportFileParser<T extends BulkImportFileElement> {

    T parse(String fileLine);
}
