package org.igramy.parser;

public class SomethingElseElement implements BulkImportFileElement {
    private String playerId;
    private String reason;

    SomethingElseElement(String playerId, String reason) {
        this.playerId = playerId;
        this.reason = reason;
    }

    public String getPlayerId() {
        return playerId;
    }

    public String getReason() {
        return reason;
    }
}
