package org.igramy.parser;

public class PointsBulkAdjustmentParser implements BulkImportFileParser{


    @Override
    public org.igramy.parser.PointsBulkAdjustmentElement parse(String fileLine) {
        String[] fields = fileLine.split(",");
        if( fields.length != 3){
            throw new RuntimeException(
                    this.getClass().getName() + " expect 3 fields in line. Found " + fields.length);
        }
        return new org.igramy.parser.PointsBulkAdjustmentElement(fields[0], Integer.valueOf(fields[1]), fields[2]);
    }
}
