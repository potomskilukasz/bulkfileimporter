package org.igramy.parser;

public class SomethingElseParser implements BulkImportFileParser {
    @Override
    public SomethingElseElement parse(String fileLine) {
        String[] fields = fileLine.split(",");
        if (fields.length != 2) {
            throw new RuntimeException(
                    this.getClass().getName() + " expect 2 fields in line. Found " + fields.length);
        }
        return new SomethingElseElement(fields[0], fields[1]);
    }
}
