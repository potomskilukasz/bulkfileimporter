package org.igramy.parser;

public class PointsBulkAdjustmentElement implements BulkImportFileElement{
    private String playerId;
    private Integer points;
    private String reason;

    PointsBulkAdjustmentElement(String playerId, Integer points, String reason) {
        this.playerId = playerId;
        this.points = points;
        this.reason = reason;
    }

    public String getPlayerId() {
        return playerId;
    }

    public Integer getPoints() {
        return points;
    }

    public String getReason() {
        return reason;
    }
}
