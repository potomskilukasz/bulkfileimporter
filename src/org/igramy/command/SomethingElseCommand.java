package org.igramy.command;

import org.igramy.parser.SomethingElseElement;

import java.util.Optional;

public class SomethingElseCommand implements BulkFileImportCommand<SomethingElseElement> {

    @Override
    public Optional<? extends CommandExceptionReport> execute(SomethingElseElement element, Long fileId) {
        System.out.println("Update processing line for file  " + fileId);
        System.out.println("Getting player account status by credentials [" + element.getPlayerId()+"]");
        System.out.println("Getting player somethingElse account status");
        System.out.println("Points assigned to player plr account / error report generated");
        SomethingElseExceptionReport exceptionReport =
                new SomethingElseExceptionReport(element.getPlayerId(), "Account Inactive");
        return Optional.of(exceptionReport);
    }
}
