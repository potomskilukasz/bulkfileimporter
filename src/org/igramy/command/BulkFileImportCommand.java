package org.igramy.command;

import org.igramy.parser.BulkImportFileElement;

import java.util.Optional;

public interface BulkFileImportCommand<T extends BulkImportFileElement> {
    Optional<? extends CommandExceptionReport> execute (T element, Long fileId);
}
