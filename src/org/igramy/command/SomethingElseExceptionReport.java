package org.igramy.command;

public class SomethingElseExceptionReport implements CommandExceptionReport {
    private String playerId;
    private String reason;

    SomethingElseExceptionReport(String playerId, String reason) {
        this.playerId = playerId;
        this.reason = reason;
    }

    @Override
    public String getAsLine() {
        return playerId+","+reason;
    }
}
