package org.igramy.command;

public interface CommandExceptionReport {
    String getAsLine();
}
