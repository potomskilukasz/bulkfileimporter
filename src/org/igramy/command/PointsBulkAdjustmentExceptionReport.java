package org.igramy.command;

public class PointsBulkAdjustmentExceptionReport implements CommandExceptionReport {

    private String playerId;
    private Integer points;
    private String reasonCode;

    public PointsBulkAdjustmentExceptionReport(String playerId, Integer points, String reasonCode) {
        this.playerId = playerId;
        this.points = points;
        this.reasonCode = reasonCode;
    }

    @Override
    public String getAsLine() {
        return playerId +","+points+","+reasonCode;
    }
}
