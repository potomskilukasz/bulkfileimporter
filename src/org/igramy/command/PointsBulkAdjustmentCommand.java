package org.igramy.command;

import org.igramy.parser.PointsBulkAdjustmentElement;

import java.util.Optional;

public class PointsBulkAdjustmentCommand implements BulkFileImportCommand<PointsBulkAdjustmentElement> {

    @Override
    public Optional<CommandExceptionReport> execute(PointsBulkAdjustmentElement element, Long fileId) {
        System.out.println("Update processing line for file  " + fileId);
        System.out.println("Getting player account status by credentials [" + element.getPlayerId()+"]");
        System.out.println("Getting player plr account status");
        System.out.println("Points assigned to player plr account / error report generated");
        SomethingElseExceptionReport exceptionReport =
                new SomethingElseExceptionReport(element.getPlayerId(), "Account Inactive");
        return Optional.of(exceptionReport);
    }
}
