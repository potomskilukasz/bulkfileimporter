package org.igramy.service;

public interface PlayersPointsBulkAdjustment {

    void assignPointsToPlayer();

    String getPlayerAccountStatus();
}
