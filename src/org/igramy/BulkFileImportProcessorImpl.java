package org.igramy;

import org.igramy.command.BulkFileImportCommand;
import org.igramy.command.CommandExceptionReport;
import org.igramy.parser.BulkImportFileParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Optional;

public class BulkFileImportProcessorImpl implements BulkFileImportProcessor {

    @SuppressWarnings("unchecked")
    @Override
    public void process(BufferedReader reader, BufferedWriter writer, BulkImportFileParser parser, BulkFileImportCommand command) throws IOException {
        String line;

        while ((line = reader.readLine()) != null) {
            Optional<CommandExceptionReport> exception = command.execute(parser.parse(line), 421L);
            if (exception.isPresent()) {
                writer.write(exception.get().getAsLine()+"\n");
            }
        }
        writer.close();
    }
}
