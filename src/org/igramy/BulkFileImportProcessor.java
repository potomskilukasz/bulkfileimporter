package org.igramy;

import org.igramy.command.BulkFileImportCommand;
import org.igramy.parser.BulkImportFileParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

public interface BulkFileImportProcessor {

    void process(BufferedReader reader, BufferedWriter writer, BulkImportFileParser parser, BulkFileImportCommand command) throws IOException;
}
