package org.igramy;

import org.igramy.command.BulkFileImportCommand;
import org.igramy.command.PointsBulkAdjustmentCommand;
import org.igramy.command.SomethingElseCommand;
import org.igramy.parser.BulkImportFileParser;
import org.igramy.parser.PointsBulkAdjustmentParser;
import org.igramy.parser.SomethingElseParser;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

public class Main {

    private static ClassLoader loader = Main.class.getClassLoader();

    public static void main(String[] args) throws IOException, URISyntaxException {
        pointAdjustmentBulkImport();
        somethingElseBulkImport();
    }

    private static void somethingElseBulkImport() throws IOException, URISyntaxException {
        BulkFileImportProcessor processor = new BulkFileImportProcessorImpl();
        URI smthElseTest = Objects.requireNonNull(loader.getResource("smthElseTest.csv")).toURI();
        URI smthElseReport = Objects.requireNonNull(loader.getResource("smthElseTestReport.csv")).toURI();


        BufferedReader reader = Files.newBufferedReader(Paths.get(smthElseTest));
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(smthElseReport), Charset.forName("UTF8"));

        BulkImportFileParser parser = new SomethingElseParser();
        BulkFileImportCommand command = new SomethingElseCommand();

        processor.process(reader, writer, parser, command);
    }


    private static void pointAdjustmentBulkImport() throws IOException, URISyntaxException {
        BulkFileImportProcessor processor = new BulkFileImportProcessorImpl();

        URI pointsBulkAdjTest = Objects.requireNonNull(loader.getResource("pointsBulkAdjTest.csv")).toURI();
        URI pointsBulkAdjTestReport = Objects.requireNonNull(loader.getResource("pointsBulkAdjTestReport.csv")).toURI();

        BufferedReader reader = Files.newBufferedReader(Paths.get(pointsBulkAdjTest), Charset.forName("UTF8"));
        BufferedWriter writer = Files.newBufferedWriter(Paths.get(pointsBulkAdjTestReport), Charset.forName("UTF8"));

        BulkImportFileParser parser = new PointsBulkAdjustmentParser();
        BulkFileImportCommand command = new PointsBulkAdjustmentCommand();

        processor.process(reader, writer, parser, command);
    }
}
